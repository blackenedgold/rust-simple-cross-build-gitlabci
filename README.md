An example project to build and cross build rust.
See [.gitlab-ci.yml](.gitlab-ci.yml) for configuration and see [releases](https://gitlab.com/blackenedgold/rust-simple-cross-build-gitlabci/releases) for the artifacts.

Note: MSVC and macOS are not supported yet.
